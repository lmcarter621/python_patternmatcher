import sys

__author__ = 'Lauren M Carter'

PATTERN_SPLITTER = ','
INPUT_SPLITTER = '/'
WILDCARD = '*'

class Matcher:

    def __init__(self, inputFile, outputFile):
        self.inputFile = inputFile
        self.outputFile = outputFile

    def inputReader(self):
        numberOfPatterns = 0
        numberOfInputs = 0
        self.patterns = {}
        self.inputs = []
        with open(self.inputFile, 'r') as i:
            numberOfPatterns = int(i.readline())
            for line in range(numberOfPatterns):
                currentLine = i.readline()
                key = currentLine.split(PATTERN_SPLITTER)[0]
                if key in self.patterns:
                    self.patterns.get(key).append(currentLine.strip('\n'))
                else:
                    self.patterns[key] = [currentLine.strip('\n')]
            numberOfInputs =  int(i.readline())
            for line in range(numberOfInputs):
                self.inputs.append(i.readline().strip('\n'))
        i.closed

        for key in self.patterns:
            self.patterns.get(key).sort(reverse=True)

    def outputWriter(self):
        matches = []

        for input in self.inputs:
            matches.append(self.findMatch(self.cleanseInput(input)))

        with open(self.outputFile, 'w') as o:
            for match in matches:
                o.write(match + '\n')
        o.closed

    def findMatch(self, input):
        eligiblePatterns = []
        currentPattern = 'NO MATCH'
        currentMatch = 0

        firstInput =input.split(INPUT_SPLITTER)[0]

        #find the closest match
        if firstInput in self.patterns:
            eligiblePatterns = self.patterns.get(firstInput)
            for pattern in eligiblePatterns:
                match = self.distance(input, pattern)
                if match > currentMatch:
                    currentPattern = pattern
                    currentMatch = match

        #check wildcards for closer matches
        eligiblePatterns = self.patterns.get(WILDCARD)
        for pattern in eligiblePatterns:
            match = self.distance(input, pattern)
            if match > currentMatch:
                currentPattern = pattern
                currentMatch = match


        return currentPattern

    def distance(self, input, pattern):

        #remove leading characters
        if input[0] == INPUT_SPLITTER:
            input = input[1:]

        if pattern[0] == PATTERN_SPLITTER:
            pattern = pattern[1:]

        #split the input & pattern into parts
        inputParts = input.split(INPUT_SPLITTER)
        patternParts = pattern.split(PATTERN_SPLITTER)
        match = 0

        #cannot be matches because they are not equal lengths
        if(len(inputParts) != len(patternParts)):
            return -1

        #calculate the matches betw the two
        for inputPart, patternPart in zip(inputParts, patternParts):
            if patternPart == WILDCARD or inputPart!= patternPart:
                continue
            else:
                match = match + 1


        return match

    def cleanseInput(self, input):

        if input[0] == '/':
            input = input[1:]

        inputLen = len(input)-1
        if input[inputLen] == '/':
            input = input[0:inputLen]

        return input

if __name__ =='__main__':
    args = sys.argv
    argumentLength = len(args)
    if(argumentLength != 3):
        sys.exit('INVALID NUMBER OF ARGUMENTS')

    matcher = Matcher(args[1], args[2])
    matcher.inputReader()
    matcher.outputWriter()

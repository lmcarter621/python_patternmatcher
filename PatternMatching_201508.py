import sys

__author__ = 'Lauren M Carter'

PATTERN_SPLITTER = ','
INPUT_SPLITTER = '/'
WILDCARD = '*'

def main():

    args = sys.argv
    argumentLength = len(args)
    if(argumentLength != 3):
        sys.exit('INVALID NUMBER OF ARGUMENTS')

    inputFile = args[1]
    outputFile = args[2]

    numberOfPatterns = 0
    numberOfInputs = 0
    patterns = {}
    inputs = []
    with open(inputFile, 'r') as i:
        numberOfPatterns = int(i.readline())
        for line in range(numberOfPatterns):
            currentLine = i.readline()
            key = currentLine.split(PATTERN_SPLITTER)[0]
            if key in patterns:
                patterns.get(key).append(currentLine.strip('\n'))
            else:
                patterns[key] = [currentLine.strip('\n')]
        numberOfInputs =  int(i.readline())
        for line in range(numberOfInputs):
            inputs.append(i.readline().strip('\n'))
    i.closed

    for key in patterns:
        patterns.get(key).sort(reverse=True)

    matches = []

    for input in inputs:
        matches.append(findMatch(cleanseInput(input), patterns, 0, len(patterns)))

    with open(outputFile, 'w') as o:
        for match in matches:
            o.write(match + '\n')
    o.closed



def distance(input, pattern):

    #remove leading characters
    if input[0] == INPUT_SPLITTER:
        input = input[1:]

    if pattern[0] == PATTERN_SPLITTER:
        pattern = pattern[1:]

    #split the input & pattern into parts
    inputParts = input.split('/')
    patternParts = pattern.split(PATTERN_SPLITTER)
    match = 0

    #cannot be matches because they are not equal lengths
    if(len(inputParts) != len(patternParts)):
        return -1

    #calculate the matches betw the two
    for inputPart, patternPart in zip(inputParts, patternParts):
        if patternPart == WILDCARD or inputPart!= patternPart:
            continue
        else:
            match = match + 1


    return match

def findMatch(input, patterns, beg, end):
    eligiblePatterns = []
    currentPattern = 'NO MATCH'
    currentMatch = 0

    firstInput =input.split(INPUT_SPLITTER)[0]

    #find the closest match
    if firstInput in patterns:
        eligiblePatterns = patterns.get(firstInput)
        for pattern in eligiblePatterns:
            match = distance(input, pattern)
            if match > currentMatch:
                currentPattern = pattern
                currentMatch = match

    #check wildcards for closer matches
    eligiblePatterns = patterns.get(WILDCARD)
    for pattern in eligiblePatterns:
        match = distance(input, pattern)
        if match > currentMatch:
            currentPattern = pattern
            currentMatch = match


    return currentPattern

def cleanseInput(input):

    if input[0] == '/':
        input = input[1:]

    inputLen = len(input)-1
    if input[inputLen] == '/':
        input = input[0:inputLen]

    return input

if __name__ =='__main__':
    sys.exit(main())